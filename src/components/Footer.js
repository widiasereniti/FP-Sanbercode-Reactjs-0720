import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        padding: '20px',
        flexGrow: 1,
    },
});

export default function Footer() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                Sanbercode{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        </div>
    );
}
