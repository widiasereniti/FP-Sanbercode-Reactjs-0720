import React, {useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles'
import { Card, Button, CssBaseline, TextField, Grid, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));

export const AddGames = () => {
    const classes = useStyles()
    const history = useHistory()

    const [inputName, setInputName] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputSinglePlayer, setInputSinglePlayer] = useState("")
    const [inputMultiplayer, setInputMultiplayer] = useState("")
    const [inputPlatform, setInputPlatform] = useState("")
    const [inputRelease, setInputRelease] = useState("")
    const [inputImage, setInputImage] = useState("")
    const [statusForm, setStatusForm]  =  useState("create")

    const handleChange = (event) =>{
        const { name, value } = event.currentTarget
        switch(name){
            case "name":
                setInputName(value);
                break;
            case "genre":
                setInputGenre(value);
                break;
            case "singlePlayer":
                setInputSinglePlayer(value);
                break;
            case "multiplayer":
                setInputMultiplayer(value);
                break;
            case "platform":
                setInputPlatform(value);
                break;
            case "release":
                setInputRelease(value);
                break;
            case "image":
                setInputImage(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let addGames = { name: inputName, genre: inputGenre, singlePlayer: inputSinglePlayer, multiplayer: inputMultiplayer, platform: inputPlatform, release: inputRelease, image_url: inputImage }

        if (addGames.name.replace(/\s/g,'') !== "" && addGames.genre.replace(/\s/g,'') !== "" &&
            addGames.singlePlayer.toString().replace(/\s/g,'') !== "" && addGames.multiplayer.toString().replace(/\s/g,'') !== "" &&
            addGames.platform.replace(/\s/g,'') !== "" && addGames.release.toString().replace(/\s/g,'') !== "" &&
            addGames.image_url.toString().replace(/\s/g,'') !== "" ){
            if (statusForm === "create"){
                axios.post(`http://backendexample.sanbercloud.com/api/games`, addGames)
                .then(res => {
                    history.push("/games")
                })

            }
            
            setStatusForm("create")
            setInputName("")
            setInputGenre("")
            setInputSinglePlayer("")
            setInputMultiplayer("")
            setInputPlatform("")
            setInputRelease("")
            setInputImage("")
        }
    }

    return (
        <Card style={{padding: '20px'}}>
            <CssBaseline />
            <Button variant="contained" color="secondary" href={`/games`} styles={{margin: '20px'}}>
                Back
            </Button>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5" style={{margin:'20px'}}>
                Add Games
                </Typography>
                <form noValidate className={classes.form} onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="name"
                        variant="outlined"
                        required
                        fullWidth
                        id="name"
                        label="Name"
                        autoFocus
                        onChange={handleChange}
                        value={inputName}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="genre"
                        label="Genre"
                        name="genre"
                        autoFocus
                        onChange={handleChange}
                        value={inputGenre}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="singlePlayer"
                        variant="outlined"
                        required
                        fullWidth
                        id="singlePlayer"
                        label="Singleplayer"
                        autoFocus
                        onChange={handleChange}
                        value={inputSinglePlayer}
                        type="number"
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="multiplayer"
                        label="Multiplayer"
                        name="multiplayer"
                        type="number"
                        autoFocus
                        onChange={handleChange}
                        value={inputMultiplayer}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="platform"
                        variant="outlined"
                        required
                        fullWidth
                        id="platform"
                        label="Platform"
                        autoFocus
                        onChange={handleChange}
                        value={inputPlatform}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="release"
                        label="Release"
                        name="release"
                        type="number"
                        autoFocus
                        onChange={handleChange}
                        value={inputRelease}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        name="image"
                        variant="outlined"
                        required
                        fullWidth
                        id="image"
                        label="Image Url"
                        autoFocus
                        onChange={handleChange}
                        value={inputImage}
                        size="small"
                    />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Submit
                </Button>
                </form>
            </div>
        </Card>
    )
}

export default AddGames