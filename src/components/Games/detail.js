import React, { Component } from 'react';
import axios from 'axios'
import './style.css'

import { Card, Button, Typography } from '@material-ui/core';

class DetailGames extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataDetailGames: ''
        }
    }

    componentDidMount(){
        // read array data
        if (this.state.dataDetailGames === '') {
            var id = this.props.match.params.id
            axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
            .then((response) => {
                const dataDetailGames = response.data;
                this.setState({ dataDetailGames });
            });
        }
    }

    render(){
        const { dataDetailGames } = this.state
        // console.log(dataDetailGames)
        return (
            <div>
                <Card style={{padding: '20px'}}>
                    <Button variant="contained" color="secondary" href={`/gameslist/`}>
                        Back
                    </Button>
                    <div className="container-img">
                    {
                        dataDetailGames ?
                            <React.Fragment>
                                <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                                    {dataDetailGames.name}{''}{'('+dataDetailGames.release+')'}
                                </Typography>
                                <Typography component="img" style={{height: 300, width: 'auto'}} src={dataDetailGames.image_url} />
                                <Typography component="h2" style={{textAlign:'center', margin: '5px'}}>
                                    <strong>Genre: </strong>{dataDetailGames.genre}<br/>
                                    <strong>Single Player: </strong>{dataDetailGames.singlePlayer}<br/>
                                    <strong>Multiplayer: </strong>{dataDetailGames.multiplayer}<br/>
                                    <strong>Platform: </strong>{dataDetailGames.platform}<br/>
                                </Typography>
                            </React.Fragment>
                        : 
                        <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                            Data Not Found
                        </Typography>
                    }
                    </div>
                </Card>
            </div>
        );
    }
}

export default DetailGames