import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { useHistory, useParams } from 'react-router-dom';
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));

export const EditGames = () => {
    const classes = useStyles()
    const history = useHistory()
    const { id } = useParams()

    const [inputName, setInputName] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputSinglePlayer, setInputSinglePlayer] = useState("")
    const [inputMultiplayer, setInputMultiplayer] = useState("")
    const [inputPlatform, setInputPlatform] = useState("")
    const [inputRelease, setInputRelease] = useState("")
    const [inputImage, setInputImage] = useState("")

    useEffect(() => {
        if (id !== '') {
            axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
            .then(res => {
                setInputName(res.data.name)
                setInputGenre(res.data.genre)
                setInputSinglePlayer(res.data.singlePlayer)
                setInputMultiplayer(res.data.multiplayer)
                setInputPlatform(res.data.platform)
                setInputRelease(res.data.release)
                setInputImage(res.data.image_url)
            })
        }

    }, [])

    const handleChange = (event) =>{
        const { name, value } = event.target
        switch(name){
            case "Name":
                setInputName(value);
                break;
            case "Genre":
                setInputGenre(value);
                break;
            case "singlePlayer":
                setInputSinglePlayer(value);
                break;
            case "Multiplayer":
                setInputMultiplayer(value);
                break;
            case "Platform":
                setInputPlatform(value);
                break;
            case "Release":
                setInputRelease(value);
                break;
            case "Image":
                setInputImage(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let addGames = { name: inputName, genre: inputGenre, singlePlayer: inputSinglePlayer, multiplayer: inputMultiplayer, platform: inputPlatform, release: inputRelease, image_url: inputImage }
        // console.log(id+"/"+addGames.name+"/"+addGames.genre+"/"+addGames.singlePlayer+"/"+addGames.multiplayer+"/"+addGames.platform+"/"+addGames.release+"/"+addGames.image_url+"/")
        if (addGames.name.replace(/\s/g,'') !== "" && addGames.genre.replace(/\s/g,'') !== "" &&
            addGames.singlePlayer.toString().replace(/\s/g,'') !== "" && addGames.multiplayer.toString().replace(/\s/g,'') !== "" &&
            addGames.platform.replace(/\s/g,'') !== "" && addGames.release.toString().replace(/\s/g,'') !== "" &&
            addGames.image_url.toString().replace(/\s/g,'') !== "" ){
                axios.put(`https://backendexample.sanbersy.com/api/games/${id} `, addGames)
                .then(res => {
                    console.log(res)
                    history.push("/games")
                })

            
            setInputName("")
            setInputGenre("")
            setInputSinglePlayer("")
            setInputMultiplayer("")
            setInputPlatform("")
            setInputRelease("")
            setInputImage("")
        }
    }

    return (
        <Card style={{padding: '20px'}}>
            <CssBaseline />
            <Button variant="contained" color="secondary" href={`/games`} styles={{margin: '20px'}}>
                Back
            </Button>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5" style={{margin:'20px'}}>
                Edit Games
                </Typography>
                <form noValidate className={classes.form} onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Name"
                        variant="outlined"
                        required
                        fullWidth
                        id="Name"
                        label="Name"
                        autoFocus
                        onChange={handleChange}
                        value={inputName}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="Genre"
                        label="Genre"
                        name="Genre"
                        autoFocus
                        onChange={handleChange}
                        value={inputGenre}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="singlePlayer"
                        variant="outlined"
                        required
                        fullWidth
                        id="singlePlayer"
                        label="Singleplayer"
                        autoFocus
                        onChange={handleChange}
                        value={inputSinglePlayer}
                        type="number"
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="Multiplayer"
                        label="Multiplayer"
                        name="Multiplayer"
                        type="number"
                        autoFocus
                        onChange={handleChange}
                        value={inputMultiplayer}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Platform"
                        variant="outlined"
                        required
                        fullWidth
                        id="Platform"
                        label="Platform"
                        autoFocus
                        onChange={handleChange}
                        value={inputPlatform}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="Release"
                        label="Release"
                        name="Release"
                        type="number"
                        autoFocus
                        onChange={handleChange}
                        value={inputRelease}
                        size="small"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        name="Image"
                        variant="outlined"
                        required
                        fullWidth
                        id="Image"
                        label="Image Url"
                        autoFocus
                        onChange={handleChange}
                        value={inputImage}
                        size="small"
                    />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Submit
                </Button>
                </form>
            </div>
        </Card>
    )
}

export default EditGames