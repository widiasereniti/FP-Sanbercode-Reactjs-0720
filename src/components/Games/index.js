import React, { Component } from 'react';
import axios from 'axios'
import './style.css'

import { Card, CardActionArea, CardActions, CardContent, CardMedia, Button, Typography } from '@material-ui/core';

class GamesList extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataGames: []
        }
    }

    componentDidMount(){
        // read array data
        if (this.state.dataGames.length === 0) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
            .then((response) => {
                const dataGames = response.data;
                this.setState({ dataGames });
            });
        }
    }

    render(){
        const { dataGames } = this.state
        return (
            <div>
                <Card style={{padding: '20px'}}>
                    <h1 style={{textAlign:'center'}}>List Games</h1>
                    <br/>
                    <div className="container-img">
                    {
                        dataGames.map((data, index) => {
                            return(
                                <Card className="card-img" key={index}>
                                    <CardActionArea>
                                        {console.log(data.image_url)}
                                        <CardMedia component="img" style={{height: 140}} src={data.image_url}/>
                                        <CardContent style={{height: 110, overflow:'hidden'}}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                {data.name}{' '}{'(' + data.release +')'}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                
                                            <strong>Genre: </strong>{data.genre}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <strong>Platform: </strong>{data.platform}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                        <Button size="small" color="primary" href={`/gameslist/detail/${data.id}`}> 
                                        Detail
                                        </Button>
                                    </CardActions>
                                </Card>
                            )
                        })
                    }
                    </div>
                </Card>
            </div>
        );
    }
}

export default GamesList