import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom";
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import { Card, Button, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const GamesTable = () => {
    const classes = useStyles();
    const [dataGames, setDataGames] = useState(null)

    useEffect( () => {
        if (dataGames === null){
        axios.get(`https://backendexample.sanbersy.com/api/games`)
            .then(res => {
                setDataGames(res.data.map(el => { 
                    return {
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singleplayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        release: el.release,
                        image: el.image
                    }
                }))
            })
        }
    }, [dataGames])


    const handleDelete = (event) => {
        const { value } = event.currentTarget
        let idGames = parseInt(value)
        
        let newDataGames = dataGames.filter(item => item.id !== idGames)
        axios.delete(`https://backendexample.sanbersy.com/api/games/${idGames}`)
        .then(res=>{
            console.log(res);
        })

        setDataGames([...newDataGames])
    }
    
    return (
        <>
            <Card style={{padding: '20px'}}>
                <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                    List Games
                </Typography>
                <Button variant="contained" startIcon={<Add />} color="primary" href={`/games/add`} style={{textAlign:"right", margin:'8px 0'}}>
                    Add Games
                </Button>

                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Nama Games</TableCell>
                            <TableCell align="center">Genre</TableCell>
                            <TableCell align="center">Singleplayer</TableCell>
                            <TableCell align="center">Multiplayer</TableCell>
                            <TableCell align="center">Platform</TableCell>
                            <TableCell align="center">Release</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {
                            dataGames !== null && dataGames.map((data, index)=>{
                                return(                    
                                    <TableRow key={index}>
                                        <TableCell>{data.name}</TableCell>
                                        <TableCell>{data.genre}</TableCell>
                                        <TableCell>{data.singleplayer}</TableCell>
                                        <TableCell>{data.multiplayer}</TableCell>
                                        <TableCell>{data.platform}</TableCell>
                                        <TableCell>{data.release}</TableCell>
                                        <TableCell style={{display:'flex'}}>
                                            <Button variant="contained" color="primary" style={{marginRight:'5px'}}><Link to={`/games/edit/${data.id}`} style={{ color: "white" }}><Edit /></Link></Button>
                                            <Button variant="contained" color="secondary" onClick={handleDelete} value={data.id}><Delete /></Button>
                                        </TableCell>
                                    </TableRow>
                                )
                            })
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Card>
        </>
    )
}

export default GamesTable