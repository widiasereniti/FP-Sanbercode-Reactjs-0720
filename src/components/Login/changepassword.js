import React, {useState, useEffect} from 'react';
import axios from 'axios'
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { Link, Grid, Avatar, Button, CssBaseline, TextField, Typography, Container } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function ChangePassword() {
  const classes = useStyles()
  const history = useHistory()
  const local = localStorage.getItem("user").split("^")
  const id = local[0]
  
  const [inputUsername, setInputUsername] = useState("")
  const [inputPassword, setInputPassword] = useState("")

  useEffect(() => {
    if (id !== '') {
        axios.get(`https://backendexample.sanbersy.com/api/users/${id}`)
        .then(res => {
            setInputUsername(res.data.username)
            setInputPassword("")
        })
    }
  }, [])

  const handleChange = (event) =>{
    const { name, value } = event.currentTarget
    switch(name){
        case "Username":
          setInputUsername(value);
            break;
        case "Password":
          setInputPassword(value);
            break;
        default:
            break;
    }
  }

  const handleSubmit = (event) =>{

    event.preventDefault()
    let editUser = { username: inputUsername, password: inputPassword}
    console.log(editUser.username+"/"+editUser.password)
    if (editUser.username.replace(/\s/g,'') !== "" && editUser.password.replace(/\s/g,'') !== ""){
        axios.put(`https://backendexample.sanbersy.com/api/users/${id}`, editUser)
        .then(res => {
            history.push("/")
        })
        
        setInputUsername("")
        setInputPassword("")
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Change Password
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="Username"
                label="Username"
                name="Username"
                autoComplete="Username"
                onChange={handleChange}
                value={inputUsername}
                disabled
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="Password"
                label="New Password"
                type="password"
                id="Password"
                autoComplete="current-password"
                onChange={handleChange}
                value={inputPassword}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Change Password
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href={`/login`} variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}