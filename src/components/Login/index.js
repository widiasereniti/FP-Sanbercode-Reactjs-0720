import React, {useState, useContext} from 'react';
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
// import {LoginContext} from "./loginContext";
import { Avatar, Button, CssBaseline, TextField, Link, Grid, Typography, Container } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();

  // const [login, setLogin] = useContext(LoginContext)
  const [inputUsername, setInputUsername] = useState("")
  const [inputPassword, setInputPassword] = useState("")

  const handleChange = (event) =>{
    const { name, value } = event.currentTarget
    switch(name){
        case "Username":
          setInputUsername(value);
            break;
        case "Password":
          setInputPassword(value);
            break;
        default:
            break;
    }
  }

  const handleSubmit = (event) =>{

    event.preventDefault()
    let username = inputUsername
    let password = inputPassword

    console.log(username+"/"+password)
    if (username !== "" && password !== "") {
        axios.post("https://backendexample.sanbersy.com/api/login", {username, password})
        .then(res => {
          console.log(res)
            if(res.status === 200){
                if(res.data === "invalid username or password"){
                    alert(res.data)
                }else{
                    // setLogin("login")
                    var id = res.data.id
                    var username = res.data.username
                    var password = res.data.password
                    localStorage.setItem("user", (id+"^"+username+"^"+password))
                }
            }
        })
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="Username"
            label="Username"
            name="Username"
            autoComplete="Username"
            autoFocus
            onChange={handleChange}
            value={inputUsername}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="Password"
            label="Password"
            type="password"
            id="Password"
            autoComplete="current-password"
            onChange={handleChange}
            value={inputPassword}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <Link href={`/register`} variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}