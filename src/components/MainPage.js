import React from 'react';
import MiniDrawerContainer from '../container/MiniDrawerContainer';
import { Routes } from '../config/Routes';
import { useStyles } from '../assets/styles';

function MainPage(props) {
    const classes = useStyles;
    const { openSideBar, toggleHandler } = props;
    
    return (
        <div className={classes.root}>
            <MiniDrawerContainer openSideBar={openSideBar} sideBarClick={toggleHandler} />
            <Routes openSideBar={openSideBar} sideBarClick={toggleHandler} />
        </div>
    );
}

export default MainPage;