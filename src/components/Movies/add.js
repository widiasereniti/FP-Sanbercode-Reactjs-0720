import React, {useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles'
import { Card, Button, CssBaseline, TextField, Grid, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));

export const AddMovies = () => {
    const classes = useStyles()
    const history = useHistory()

    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [inputReview, setInputReview] = useState("")
    const [inputImage, setInputImage] = useState("")
    const [statusForm, setStatusForm]  =  useState("create")

    const handleChange = (event) =>{
        const { name, value } = event.currentTarget
        switch(name){
            case "Title":
                setInputTitle(value);
                break;
            case "Description":
                setInputDescription(value);
                break;
            case "Year":
                setInputYear(value);
                break;
            case "Duration":
                setInputDuration(value);
                break;
            case "Genre":
                setInputGenre(value);
                break;
            case "Rating":
                setInputRating(value);
                break;
            case "Review":
                setInputReview(value);
                break;
            case "Image":
                setInputImage(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let addMovie = { title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating, review: inputReview, image_url: inputImage }
        // console.log(title+"/"+description+"/"+year+"/"+duration+"/"+genre+"/"+rating+"/"+review+"/"+image_url)

        if (addMovie.title.replace(/\s/g,'') !== "" && addMovie.description.replace(/\s/g,'') !== "" &&
            addMovie.year.toString().replace(/\s/g,'') !== "" && addMovie.duration.toString().replace(/\s/g,'') !== "" &&
            addMovie.genre.replace(/\s/g,'') !== "" && addMovie.rating.toString().replace(/\s/g,'') !== "" &&
            addMovie.review.replace(/\s/g,'') !== "" && addMovie.image_url.toString().replace(/\s/g,'')
            ){
            if (statusForm === "create"){
                axios.post(`https://backendexample.sanbersy.com/api/movies`, addMovie)
                .then(res => {
                    history.push("/movies")
                })
    
            }
            
            setStatusForm("create")
            setInputTitle("")
            setInputDescription("")
            setInputYear("")
            setInputDuration("")
            setInputGenre("")
            setInputRating("")
            setInputReview("")
            setInputImage("")
        }
    }

    return (
        <Card style={{padding: '20px'}}>
            <CssBaseline />
            <Button variant="contained" color="secondary" href={`/Movies`} styles={{margin: '20px'}}>
                Back
            </Button>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5" style={{margin:'20px'}}>
                Add Movies
                </Typography>
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Title"
                        variant="outlined"
                        id="Title"
                        label="Title"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputTitle}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Year"
                        variant="outlined"
                        id="Year"
                        label="Year"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputYear}
                        type="number"
                    />
                    </Grid>                    
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        id="Duration"
                        label="Duration"
                        name="Duration"
                        type="number"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputDuration}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Genre"
                        variant="outlined"
                        id="Genre"
                        label="Genre"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputGenre}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        id="Rating"
                        label="Rating"
                        name="Rating"
                        type="number"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputRating}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Description"
                        label="Description"
                        name="Description"
                        autoFocus
                        required
                        fullWidth
                        multiline
                        rows={4}
                        onChange={handleChange}
                        size="small"
                        value={inputDescription}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Review"
                        label="Review"
                        name="Review"
                        autoFocus
                        required
                        fullWidth
                        multiline
                        rows={4}
                        onChange={handleChange}
                        size="small"
                        value={inputReview}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Image"
                        label="Image Url"
                        name="Image"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputImage}
                    />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Submit
                </Button>
                </form>
            </div>
        </Card>
    )
}

export default AddMovies