import React, { Component } from 'react';
import axios from 'axios'
import './style.css'

import { Card, Button, Typography } from '@material-ui/core';

class DetailMovies extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataDetailMovies: ''
        }
    }

    componentDidMount(){
        // read array data
        if (this.state.dataDetailMovies === '') {
            var id = this.props.match.params.id
            axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
            .then((response) => {
                const dataDetailMovies = response.data;
                this.setState({ dataDetailMovies });
            });
        }
    }

    render(){
        const { dataDetailMovies } = this.state
        // console.log(dataDetailMovies)
        return (
            <div>
                <Card style={{padding: '20px'}}>
                    <Button variant="contained" color="secondary" href={`/movieslist/`}>
                        Back
                    </Button>
                    <div className="container-img">
                    {
                        dataDetailMovies ?
                            <React.Fragment>
                                <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                                    {dataDetailMovies.title}{''}{'('+dataDetailMovies.year+')'}
                                </Typography>
                                <Typography component="img" style={{height: 300, width: 'auto'}} src={dataDetailMovies.image_url} />
                                <Typography component="h2" style={{textAlign:'center', margin: '5px'}}>
                                    <strong>Genre: </strong>{dataDetailMovies.genre}<br/>
                                    <strong>Rating: </strong>{dataDetailMovies.rating}<br/>
                                    <strong>Duration: </strong>{dataDetailMovies.duration}<br/>
                                    <strong>Description: </strong><br/>
                                    {dataDetailMovies.description}<br/><br/>
                                    <strong>Review: </strong><br/>
                                    {dataDetailMovies.review}<br/>                                    
                                </Typography>
                            </React.Fragment>
                        : 
                        <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                            Data Not Found
                        </Typography>
                    }
                    </div>
                </Card>
            </div>
        );
    }
}

export default DetailMovies 