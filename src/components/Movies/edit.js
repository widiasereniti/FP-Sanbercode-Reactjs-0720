import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory, useParams } from 'react-router-dom';
import { Card, Button, CssBaseline, TextField, Grid, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));

export const EditMovies = () => {
    const classes = useStyles()
    const history = useHistory()
    const { id } = useParams()
    
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [inputReview, setInputReview] = useState("")
    const [inputImage, setInputImage] = useState("")

    useEffect(() => {
        if (id !== '') {
            axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
            .then(res => {
                setInputTitle(res.data.title)
                setInputDescription(res.data.description)
                setInputYear(res.data.year)
                setInputDuration(res.data.duration)
                setInputGenre(res.data.genre)
                setInputRating(res.data.rating)
                setInputReview(res.data.review)
                setInputImage(res.data.image_url)
            })
        }
    }, [])

    const handleChange = (event) =>{
        const { name, value } = event.currentTarget
        switch(name){
            case "Title":
                setInputTitle(value);
                break;
            case "Description":
                setInputDescription(value);
                break;
            case "Year":
                setInputYear(value);
                break;
            case "Duration":
                setInputDuration(value);
                break;
            case "Genre":
                setInputGenre(value);
                break;
            case "Rating":
                setInputRating(value);
                break;
            case "Review":
                setInputReview(value);
                break;
            case "Image":
                setInputImage(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let addMovie = { title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating, review: inputReview, image_url: inputImage }
        console.log(addMovie.title+"/"+addMovie.description+"/"+addMovie.year+"/"+addMovie.duration+"/"+addMovie.genre+"/"+addMovie.rating+"/"+addMovie.review+"/"+addMovie.image_url)

        if (addMovie.title.replace(/\s/g,'') !== "" && addMovie.description.replace(/\s/g,'') !== "" &&
            addMovie.year.toString().replace(/\s/g,'') !== "" && addMovie.duration.toString().replace(/\s/g,'') !== "" &&
            addMovie.genre.replace(/\s/g,'') !== "" && addMovie.rating.toString().replace(/\s/g,'') !== "" &&
            addMovie.review.replace(/\s/g,'') !== "" && addMovie.image_url.toString().replace(/\s/g,'')
            ){
                axios.put(`https://backendexample.sanbersy.com/api/movies/${id} `, addMovie)
                .then(res => {
                    console.log(res)
                    history.push("/movies")
                })
            
            setInputTitle("")
            setInputDescription("")
            setInputYear("")
            setInputDuration("")
            setInputGenre("")
            setInputRating("")
            setInputReview("")
            setInputImage("")
        }
    }

    return (
        <Card style={{padding: '20px'}}>
            <CssBaseline />
            <Button variant="contained" color="secondary" href={`/Movies`} styles={{margin: '20px'}}>
                Back
            </Button>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5" style={{margin:'20px'}}>
                Edit Movies
                </Typography>
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Title"
                        variant="outlined"
                        id="Title"
                        label="Title"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputTitle}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Year"
                        variant="outlined"
                        id="Year"
                        label="Year"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputYear}
                        type="number"
                    />
                    </Grid>                    
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        id="Duration"
                        label="Duration"
                        name="Duration"
                        type="number"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputDuration}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        name="Genre"
                        variant="outlined"
                        id="Genre"
                        label="Genre"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputGenre}
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"
                        id="Rating"
                        label="Rating"
                        name="Rating"
                        type="number"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputRating}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Description"
                        label="Description"
                        name="Description"
                        autoFocus
                        required
                        fullWidth
                        multiline
                        rows={4}
                        onChange={handleChange}
                        size="small"
                        value={inputDescription}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Review"
                        label="Review"
                        name="Review"
                        autoFocus
                        required
                        fullWidth
                        multiline
                        rows={4}
                        onChange={handleChange}
                        size="small"
                        value={inputReview}
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        id="Image"
                        label="Image Url"
                        name="Image"
                        autoFocus
                        required
                        fullWidth
                        onChange={handleChange}
                        size="small"
                        value={inputImage}
                    />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Submit
                </Button>
                </form>
            </div>
        </Card>
    )
}

export default EditMovies