import React, { Component } from 'react';
import axios from 'axios'
import './style.css'

import { Card, CardActionArea, CardActions, CardContent, CardMedia, Button, Typography } from '@material-ui/core';

class MoviesList extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataMovies: []
        }
    }

    componentDidMount(){
        // read array data
        if (this.state.dataMovies.length === 0) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
            .then((response) => {
                const dataMovies = response.data;
                this.setState({ dataMovies });
            });
        }
    }

    render(){
        const { dataMovies } = this.state
        return (
            <div>
                <Card style={{padding: '20px'}}>
                    <h1 style={{textAlign:'center'}}>List Movies</h1>
                    <br/>
                    <div className="container-img">
                    {
                        dataMovies.map((data, index) => {
                            return(
                                <Card className="card-img" key={index}>
                                    <CardActionArea>
                                        {console.log(data.image_url)}
                                        <CardMedia component="img" style={{height: 140, overflow: 'hidden'}} src={data.image_url}/>
                                        <CardContent style={{height: 110, overflow:'hidden'}}>
                                            <Typography gutterBottom component="h6">
                                                <strong style={{color:'black'}}>{data.title}{' '}{'(' + data.year +')'}</strong>
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                
                                            <strong>Genre: </strong>{data.genre}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <strong>Rating: </strong>{data.rating}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                        <Button size="small" color="primary" href={`/movieslist/detail/${data.id}`}> 
                                        Detail
                                        </Button>
                                    </CardActions>
                                </Card>
                            )
                        })
                    }
                    </div>
                </Card>
            </div>
        );
    }
}

export default MoviesList