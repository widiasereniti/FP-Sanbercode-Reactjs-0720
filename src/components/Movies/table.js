import React, {useEffect, useState} from 'react';
import axios from 'axios'
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import { Card, Button, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import { TableSortLabel } from '@material-ui/core';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const MoviesTable = () => {
    const classes = useStyles();
    const [dataMovies, setDataMovies] = useState(null)

    useEffect( () => {
        if (dataMovies === null){
        axios.get(`https://backendexample.sanbersy.com/api/movies`)
            .then(res => {
                setDataMovies(res.data.map(el => { 
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating,
                        review: el.review,
                        image: el.image
                    }
                }))
            })
        }
    }, [dataMovies])


    const handleDelete = (event) => {
        const { value } = event.currentTarget
        let idMovies = parseInt(value)
        
        let newDataMovies = dataMovies.filter(item => item.id !== idMovies)
        axios.delete(`https://backendexample.sanbersy.com/api/Movies/${idMovies}`)
        .then(res=>{
            console.log(res);
        })

        setDataMovies([...newDataMovies])
    }
    
    return (
        <>
            <Card style={{padding: '20px'}}>
                <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                    List Movies
                </Typography>
                <Button variant="contained" startIcon={<Add />} color="primary" href={`/movies/add`} style={{textAlign:"right", margin:'8px 0'}}>
                    Add Movies
                </Button>

                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Nama Movies</TableCell>
                            <TableCell align="center">Year</TableCell>
                            <TableCell align="center">Duration</TableCell>
                            <TableCell align="center">Genre</TableCell>
                            <TableCell align="center">Rating</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {
                            dataMovies !== null && dataMovies.map((data, index)=>{
                                return(                    
                                    <TableRow key={index}>
                                        <TableCell>{data.title}</TableCell>
                                        <TableCell>{data.year}</TableCell>
                                        <TableCell>{data.duration}</TableCell>
                                        <TableCell>{data.genre}</TableCell>
                                        <TableCell>{data.rating}</TableCell>
                                        <TableCell style={{display:'flex'}}>
                                            <Button variant="contained" color="primary" style={{marginRight:'5px'}}><Link to={`/movies/edit/${data.id}`} style={{ color: "white" }}><Edit /></Link></Button>
                                            <Button variant="contained" color="secondary" onClick={handleDelete} value={data.id}><Delete /></Button>
                                        </TableCell>
                                    </TableRow>
                                )
                            })
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Card>
        </>
    )
}

export default MoviesTable