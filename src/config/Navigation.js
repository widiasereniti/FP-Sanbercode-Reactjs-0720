import React from 'react';
import GamesList from '../components/Games/index';
import GamesTable from '../components/Games/table';
import MoviesList from '../components/Movies/index';
import MoviesTable from '../components/Movies/table';
import Login from '../components/Login/index'
import ChangePassword from '../components/Login/changepassword'

import { Games, QueueMusic, Movie, LocalMovies, ExitToApp, Lock } from "@material-ui/icons";

export const Navigation = [
    {
        id:1,
        menuName: 'Games List',
        path: '/gameslist',
        component: GamesList,
        exact: true,
        icon: <Games color="primary"/>,
        submenu: []
    },
    {
        id:2,
        menuName: 'Games Table',
        path: '/games',
        component: GamesTable,
        exact: true,
        icon: <QueueMusic color="primary"/>,
        submenu: []
    },
    {
        id:3,
        menuName: 'Movies List',
        path: '/movieslist',
        component: MoviesList,
        exact: true,
        icon: <Movie color="primary"/>,
        submenu: []
    },
    {
        id:4,
        menuName: 'Movies Table',
        path: '/movies',
        component: MoviesTable,
        exact: true,
        icon: <LocalMovies color="primary"/>,
        submenu: []
    },
    {
        id:5,
        menuName: 'Change Password',
        path: '/changepassword',
        component: ChangePassword,
        exact: true,
        icon: <Lock color="primary"/>,
        submenu: []
    },
    {
        id:6,
        menuName: 'Login',
        path: '/login',
        component: Login,
        exact: true,
        icon: <ExitToApp color="primary"/>,
        submenu: []
    },
];