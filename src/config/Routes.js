import React from 'react'
import { Switch, Route } from 'react-router-dom'
import clsx from 'clsx'
import Footer from '../components/Footer'
import { useStyles } from '../assets/styles'

// import { Home } from '../container/Home'
import GamesList from '../components/Games/index'
import GamesDetail from '../components/Games/detail'
import GamesTable from '../components/Games/table'
import AddGames from '../components/Games/add'
import EditGames from '../components/Games/edit'
import MoviesList from '../components/Movies/index'
import MoviesDetail from '../components/Movies/detail'
import MoviesTable from '../components/Movies/table'
import AddMovies from '../components/Movies/add'
import EditMovies from '../components/Movies/edit'
import Login from '../components/Login/index'
import Register from '../components/Login/register'
import ChangePassword from '../components/Login/changepassword'

export const Routes = (props) => {
    const { content, contentShift } = useStyles()
    const { openSideBar } = props

    return (
        <main className={clsx(content, {
            [contentShift]: openSideBar,
        })}>
            <Switch>
                <Route exact path='/' component={GamesList} />
                <Route path='/gameslist' exact component={GamesList} />
                <Route path='/gameslist/detail/:id' component={GamesDetail} />
                <Route path='/games' exact component={GamesTable} />
                <Route path='/games/add' component={AddGames} />
                <Route path='/games/edit/:id' exact component={EditGames} />
                <Route path='/movieslist' exact component={MoviesList} />
                <Route path='/movieslist/detail/:id' component={MoviesDetail} />
                <Route path='/movies' exact component={MoviesTable} />
                <Route path='/movies/add' component={AddMovies} />
                <Route path='/movies/edit/:id' exact component={EditMovies} />
                <Route path='/login' exact component={Login} />
                <Route path='/register' exact component={Register} />
                <Route path='/changepassword' exact component={ChangePassword} />
            </Switch>
            <Footer />
        </main>
    )
}